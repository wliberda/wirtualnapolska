package com.wirtualnapolska.vo
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}