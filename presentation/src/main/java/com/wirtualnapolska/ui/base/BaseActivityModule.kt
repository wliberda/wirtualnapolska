package com.wirtualnapolska.ui.base

import android.app.Activity
import com.wirtualnapolska.di.qualifiers.ForActivity
import com.wirtualnapolska.di.scopes.PerActivity
import dagger.Binds
import dagger.Module
import android.content.Context
@Module
abstract class BaseActivityModule {

    @PerActivity
    @ForActivity
    @Binds
    abstract fun bindContext(activity: Activity): Context
}