package com.wirtualnapolska.ui.landing

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import domain.quiz.usecase.GetAllQuizzesUseCase

@Suppress("UNCHECKED_CAST")
class LandingViewActivityFactory(private val getAllQuizzesUseCase: GetAllQuizzesUseCase): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LandingViewModel::class.java)) {
            return LandingViewModel(getAllQuizzesUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")    }
}