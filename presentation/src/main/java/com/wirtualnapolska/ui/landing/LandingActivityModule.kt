package com.wirtualnapolska.ui.landing

import com.wirtualnapolska.di.scopes.PerActivity
import com.wirtualnapolska.ui.base.BaseActivityModule
import dagger.Module
import dagger.Provides
import domain.quiz.QuizRepository
import domain.quiz.usecase.GetAllQuizzesUseCase

@Module(includes = [BaseActivityModule::class])
class LandingActivityModule {




    @Provides
    @PerActivity
    fun provideGetAllQuizzesUseCase(quizRepository: QuizRepository) = GetAllQuizzesUseCase(quizRepository)


    @Provides
    @PerActivity
    fun provideLandingViewModel(getAllQuizzesUseCase: GetAllQuizzesUseCase) = LandingViewActivityFactory(getAllQuizzesUseCase)
}