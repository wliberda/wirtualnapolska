package com.wirtualnapolska.ui.landing

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import com.wirtualnapolska.R
import com.wirtualnapolska.ui.base.BaseActivity
import com.wirtualnapolska.vo.Status
import domain.quiz.Quiz
import kotlinx.android.synthetic.main.activity_landing.*
import timber.log.Timber
import javax.inject.Inject

class LandingActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: LandingViewActivityFactory
    lateinit var viewModel: LandingViewModel
    lateinit var rvAdapter: QuizAdapter

    override fun getLayoutId(): Int = R.layout.activity_landing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LandingViewModel::class.java)
        setObserver()
    }

    private fun setObserver() {
        viewModel.listOfQuizzes.observe(this, Observer {
            when(it?.status) {
                Status.ERROR -> {
                    progressBar.visibility = GONE
                    showToastMessage(R.string.error_something_wrongs)
                }
                Status.SUCCESS -> {
                    progressBar.visibility = GONE
                    setRecyclerViewAdapter(it.data)

                }
                Status.LOADING -> {
                    progressBar.visibility = VISIBLE
                }
            }
        })
    }

    private fun setRecyclerViewAdapter(data: List<Quiz>?) {
        data?.let {
            rvAdapter = QuizAdapter(it, this)
            rvAdapter.itemClick.subscribe {
                Timber.d("Quiz clicked $it")
            }
            recyclerView.apply {
                addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
                layoutManager = LinearLayoutManager(context)
                adapter = rvAdapter
            }
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.getQuizzes()
    }
}