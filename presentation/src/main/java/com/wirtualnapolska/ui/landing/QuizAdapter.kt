package com.wirtualnapolska.ui.landing

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.wirtualnapolska.R
import domain.quiz.Quiz
import io.reactivex.subjects.PublishSubject
import android.content.Context
class QuizAdapter(val data: List<Quiz>, val ctx: Context) : RecyclerView.Adapter<QuizAdapter.QuizViewHolder>(){
    val itemClick = PublishSubject.create<Quiz>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizViewHolder =
        LayoutInflater
                .from(parent.context)
                .inflate(R.layout.adapter_quiz, parent, false)
                .run {
                    QuizViewHolder(this)
        }


    override fun getItemCount(): Int = data.size



    override fun onBindViewHolder(holder: QuizViewHolder, position: Int) {
        holder.apply {
            val quiz = data[position]
            tvTitle.text = quiz.title
            tvDescription.text = quiz.content
            Glide.with(ctx)
                    .load(quiz.mainPhoto?.url)
                    .into(ivPhoto)
        }
    }

    inner class QuizViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle = view.findViewById<TextView>(R.id.tvTitle)!!
        val tvDescription = view.findViewById<TextView>(R.id.tvDescription)!!
        val ivPhoto = view.findViewById<ImageView>(R.id.ivPhoto)!!
        init {
            itemView.setOnClickListener { itemClick.onNext(data[adapterPosition]) }
        }
    }



}