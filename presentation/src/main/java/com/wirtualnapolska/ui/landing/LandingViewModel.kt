package com.wirtualnapolska.ui.landing

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.wirtualnapolska.vo.Response
import domain.quiz.Quiz
import domain.quiz.usecase.GetAllQuizzesUseCase
import io.reactivex.observers.DisposableSingleObserver
import timber.log.Timber

class LandingViewModel(private val getAllQuizzesUseCase: GetAllQuizzesUseCase) : ViewModel() {

    val listOfQuizzes = MutableLiveData<Response<List<Quiz>>>()

    fun getQuizzes() {
        listOfQuizzes.value = Response.loading()
        getAllQuizzesUseCase.execute(DisposableObserver(), null)
    }

    inner class DisposableObserver: DisposableSingleObserver<List<Quiz>>() {
        override fun onSuccess(t: List<Quiz>) {
            listOfQuizzes.value = Response.success(t)
            Timber.d("Size ${t.size}")
        }

        override fun onError(e: Throwable) {
            listOfQuizzes.value = Response.error(e)
            Timber.e("Error  $e")
        }
    }

}