package com.wirtualnapolska.di

import com.wirtualnapolska.di.scopes.PerActivity
import com.wirtualnapolska.ui.landing.LandingActivity
import com.wirtualnapolska.ui.landing.LandingActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {


    @PerActivity
    @ContributesAndroidInjector(modules = [LandingActivityModule::class])
    abstract fun contributeLandingActivity(): LandingActivity
}