package com.wirtualnapolska.di

import android.content.Context
import com.data.db.WpDb
import com.data.network.QuizAPI
import com.data.quizdata.QuizDAO
import com.data.quizdata.QuizModelMapperImpl
import com.data.quizdata.QuizRepositoryImpl
import com.wirtualnapolska.di.qualifiers.ForApplication
import dagger.Module
import dagger.Provides
import domain.quiz.QuizRepository
import javax.inject.Singleton

@Module
class DBModule {

    @Singleton
    @Provides
    fun provideDatabase(@ForApplication context: Context) = WpDb.getInstance(context)!!

    @Singleton
    @Provides
    fun provideQuizDao(wpDb: WpDb) = wpDb.quizDAO()



    @Singleton
    @Provides
    fun provideQuizMapper() = QuizModelMapperImpl()

    @Singleton
    @Provides
    fun provideQuizRepository(quizDAO: QuizDAO,
                              quizAPI: QuizAPI,
                              mapperImpl: QuizModelMapperImpl): QuizRepository = QuizRepositoryImpl(quizDAO, quizAPI, mapperImpl)
}