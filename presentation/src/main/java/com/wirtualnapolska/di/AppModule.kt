package com.wirtualnapolska.di

import android.app.Application
import android.content.Context
import com.wirtualnapolska.di.qualifiers.ForApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [(NetworkModule::class), (ActivityBuilder::class), (DBModule::class)])
class AppModule {

    @Singleton
    @Provides
    @ForApplication
    fun provideContext(app: Application): Context = app.applicationContext
}