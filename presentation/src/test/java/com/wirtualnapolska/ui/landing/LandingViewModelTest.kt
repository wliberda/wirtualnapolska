package com.wirtualnapolska.ui.landing

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.*
import com.wirtualnapolska.vo.Response
import com.wirtualnapolska.vo.Status
import domain.quiz.Quiz
import com.data.factory.QuizFactory.Factory.makeQuizList
import domain.quiz.usecase.GetAllQuizzesUseCase
import io.reactivex.observers.DisposableSingleObserver
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class LandingViewModelTest {
    @get:Rule
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var testSubject: LandingViewModel
    private val getAllQuizzesUseCase: GetAllQuizzesUseCase = mock()

    lateinit var argumentCaptor: KArgumentCaptor<DisposableSingleObserver<List<Quiz>>>
    @Before
    fun setUp() {
        testSubject = LandingViewModel(getAllQuizzesUseCase)
        argumentCaptor = argumentCaptor()
    }

    @Test
    fun `subject cannot be null`() {
        assertNotNull(testSubject)
    }

    @Test
    fun `should execute UseCase`() {
        testSubject.getQuizzes()
        testLoading(testSubject.listOfQuizzes.value!!)
        verify(getAllQuizzesUseCase, times(1)).execute(any(), anyOrNull())
        verifyNoMoreInteractions(getAllQuizzesUseCase)
    }

    @Test
    fun `should set list when success`() {
        val list = makeQuizList(5)
        testSubject.getQuizzes()
        testLoading(testSubject.listOfQuizzes.value!!)
        verify(getAllQuizzesUseCase, times(1)).execute(argumentCaptor.capture(), anyOrNull())
        argumentCaptor.firstValue.onSuccess(list)
        testSubject.listOfQuizzes.value!!.apply {
            assertTrue(status == Status.SUCCESS)
            assertTrue(data == list)
            assertTrue(error == null)
        }
        verifyNoMoreInteractions(getAllQuizzesUseCase)
    }

    @Test
    fun `should set error when failed`() {
        testSubject.getQuizzes()
        testLoading(testSubject.listOfQuizzes.value!!)
        verify(getAllQuizzesUseCase, times(1)).execute(argumentCaptor.capture(), anyOrNull())
        argumentCaptor.firstValue.onError(Throwable())
        testSubject.listOfQuizzes.value!!.apply {
            assertTrue(status == Status.ERROR)
            assertTrue(data == null)
            assertTrue(error is Throwable)
        }
        verifyNoMoreInteractions(getAllQuizzesUseCase)
    }

    private fun testLoading(response: Response<List<Quiz>>){
        response.apply {
            assertTrue(status == Status.LOADING)
            assertTrue(data == null)
            assertTrue(error == null)
        }
    }

}