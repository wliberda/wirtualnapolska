package com.data.factory

import domain.category.Category
import domain.image.Image
import domain.quiz.Quiz
import com.data.factory.DataFactory.Factory.randomBoolean
import com.data.factory.DataFactory.Factory.randomInt
import com.data.factory.DataFactory.Factory.randomLong
import com.data.factory.DataFactory.Factory.randomString

class QuizFactory {

    companion object Factory {
        fun makeQuizList(count: Int): List<Quiz> {
            val list = mutableListOf<Quiz>()
            repeat(count, {
                list.add(makeQuiz())
            })
            return list
        }

        fun makeQuiz() = Quiz(
                randomString(),
                randomString(),
                randomInt(),
                randomString(),
                randomBoolean(),
                mutableListOf(),
                randomLong(),
                randomString(),
                randomString(),
                randomString(),
                Image(),
                mutableListOf(),
                Category()
        )
    }
}