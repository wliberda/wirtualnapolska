package com.data.quizdata

import com.data.categoriesdata.CategoriesEntity
import com.data.categoriesdata.QuizCategoriesEntity
import com.data.network.QuizAPI
import com.data.tag.QuizTagEntity
import domain.quiz.Quiz
import domain.quiz.QuizDetail
import domain.quiz.QuizRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

class QuizRepositoryImpl(private val quizDAO: QuizDAO,
                         private val quizAPI: QuizAPI,
                         private val mapper: QuizModelMapperImpl) : QuizRepository{

    override fun getAllQuiz(): Single<List<Quiz>> =
        Single.concatArray(getCachedQuizzes(),
            getRemoteQuizzes()
        )
                .subscribeOn(Schedulers.io())
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
//                .toSingle()
                .singleOrError()
//                .single(mutableListOf())



    private fun getCachedQuizzes() =
            quizDAO.findAllQuizzes()
                    .map {
                        it.forEach {
                            it.categories = quizDAO.getCategoriesForQuiz(it.id)
                            it.tags = quizDAO.getTagsForQuiz(it.id)

                        }
                        it
                    }
                    .map {
                        it.map(mapper::fromEntity)
                    }



    private fun getRemoteQuizzes() =
            quizAPI.getAllQuizzes()
                    .map { it.body()!!.items }
                    .doOnNext {
                        storeInDB(it!!)
                    }
                    .map { it.map(mapper::fromEntity) }
                    .singleOrError()



    private fun storeInDB(list: List<QuizEntity>) {
        Completable.fromCallable {
            list.forEach { quiz ->
                quizDAO.insertOrUpdate(quiz)
                quiz.categories?.forEach {
                    quizDAO.insertCategories(it)
                    QuizCategoriesEntity(quiz.id, it.uid!!).let { quizDAO.insertCategoriesQuiz(it) }
                }
                quiz.tags?.forEach {
                    quizDAO.insertTag(it)
                    QuizTagEntity(quiz.id, it.uid!!).let { quizDAO.insertTagQuiz(it) }
                }
            }
        }.subscribeOn(Schedulers.io())
                .subscribe { Timber.d("Inserted quizzes ${list.size} to DB") }
    }

    override fun getQuiz(id: Long): Maybe<QuizDetail> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}