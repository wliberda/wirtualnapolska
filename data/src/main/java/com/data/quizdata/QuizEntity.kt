package com.data.quizdata

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.data.categoriesdata.CategoriesEntity
import com.data.categorydata.CategoryEntity
import com.data.celebritydata.CelebrityEntity
import com.data.imagedata.ImageEntity
import com.data.latestresultdata.LatestResultEntity
import com.data.questiondata.QuestionEntity
import com.data.ratedata.RateEntity
import com.data.sponsoredresultdata.SponsoredResultEntity
import com.data.tag.TagEntity
import domain.categories.Categories
import domain.category.Category
import domain.image.Image
import domain.tag.Tag


@Entity(tableName = "quiz")
open class QuizEntity(@PrimaryKey
                      var id: Long,
                      var buttonStart: String? = null,
                      var shareTitle: String? = null,
                      var questions: Int? = null,
                      var createdAt: String? = null,
                      var sponsored: Boolean? = null,
                      var title: String? = null,
                      var type: String? = null,
                      var content: String? = null,
                      @Embedded
                      var mainPhoto: ImageEntity? = null
                     ) {
    @Ignore
    var categories: List<CategoriesEntity>? = null
    @Ignore
    var tags: List<TagEntity>? = null
    @Ignore
    var category: CategoryEntity? = null
//    @Ignore
//    var questions: List<QuestionEntity>? = null
//    @Ignore
//    var rates: List<RateEntity>? = null
//    @Ignore
//    var category: CategoryEntity? = null
//    @Ignore
//    var categories: List<CategoriesEntity>? = null
//    @Ignore
//    var mainPhoto: ImageEntity? = null
//    @Ignore
//    var latestResult: LatestResultEntity? = null
//    @Ignore
//    var sponsoredResults: SponsoredResultEntity? = null
//    @Ignore
//    var celebrity: CelebrityEntity? = null
}