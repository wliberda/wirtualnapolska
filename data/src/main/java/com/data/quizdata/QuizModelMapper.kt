package com.data.quizdata

import com.data.categoriesdata.CategoriesEntity
import com.data.categorydata.CategoryEntity
import com.data.tag.TagEntity
import domain.categories.Categories
import domain.category.Category
import domain.quiz.Quiz
import domain.tag.Tag

interface QuizModelMapper {

    fun toEntity(quiz: Quiz): QuizEntity
    fun fromEntity(quizEntity: QuizEntity): Quiz
    fun toEntity(categories: Categories): CategoriesEntity
    fun fromEntity(categoriesEntity: CategoriesEntity): Categories
    fun toEntity(tag: Tag): TagEntity
    fun fromEntity(tagEntity: TagEntity): Tag
    fun toEntity(category: Category): CategoryEntity
    fun fromEntity(categoryEntity: CategoryEntity): Category
}