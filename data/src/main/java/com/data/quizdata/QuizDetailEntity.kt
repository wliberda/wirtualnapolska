package com.data.quizdata

import android.arch.persistence.room.Ignore
import com.data.categoriesdata.CategoriesEntity
import com.data.categorydata.CategoryEntity
import com.data.celebritydata.CelebrityEntity
import com.data.imagedata.ImageEntity
import com.data.latestresultdata.LatestResultEntity
import com.data.questiondata.QuestionEntity
import com.data.ratedata.RateEntity
import com.data.sponsoredresultdata.SponsoredResultEntity
import com.data.tag.TagEntity

data class QuizDetailEntity(var createdAt: Long? = null,
                            var sponsored: Boolean? = null,
                            var title: String? = null,
                            var type: String? = null,
                            var content: String? = null,
                            var buttonStart: String? = null,
                            var shareTitle: String? = null,
                            var id: Long? = null,
                            var scripts: String? = null,
                            var created: Int? = null,
                            var avgResult: Double? = null,
                            var resultCount: Int? = null,
                            var userBattleDone: Boolean? = null,
                            var isBattle: Boolean? = null) {


    @Ignore
    var tags: List<TagEntity>? = null
    @Ignore
    var questions: List<QuestionEntity>? = null
    @Ignore
    var rates: List<RateEntity>? = null
    @Ignore
    var category: CategoryEntity? = null
    @Ignore
    var categories: List<CategoriesEntity>? = null
    @Ignore
    var mainPhoto: ImageEntity? = null
    @Ignore
    var latestResult: List<LatestResultEntity>? = null
    @Ignore
    var sponsoredResults: SponsoredResultEntity? = null
    @Ignore
    var celebrity: CelebrityEntity? = null
}