package com.data.quizdata

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.data.base.BaseDAO
import com.data.categoriesdata.CategoriesEntity
import com.data.categoriesdata.QuizCategoriesEntity
import com.data.categorydata.CategoryEntity
import com.data.categorydata.QuizCategoryEntity
import com.data.tag.QuizTagEntity
import com.data.tag.TagEntity
import domain.quiz.Quiz
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.*

@Dao
interface QuizDAO : BaseDAO<QuizEntity> {
    @Query("SELECT * FROM quiz WHERE id = :id")
    fun findQuiz(id: Int): Maybe<QuizEntity>

    @Query("SELECT * FROM quiz")
    fun findAllQuizzes(): Single<List<QuizEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategories(categoriesEntity: CategoriesEntity)

    @Query("SELECT * FROM categories " +
            "INNER JOIN quiz_categories_join ON categories.uid=quiz_categories_join.categoriesId WHERE " +
            "quiz_categories_join.quizId=:quizId")
    fun getCategoriesForQuiz(quizId: Long): List<CategoriesEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategoriesQuiz(quizCategoriesEntity: QuizCategoriesEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTag(tagEntity: TagEntity)

    @Query("SELECT * FROM tag " +
            "INNER JOIN quiz_tag_join ON tag.uid=quiz_tag_join.tagId WHERE " +
            "quiz_tag_join.quizId=:quizId")
    fun getTagsForQuiz(quizId: Long): List<TagEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTagQuiz(quizTagEntity: QuizTagEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(categoryEntity: CategoryEntity)

    @Query("SELECT * FROM category " +
            "INNER JOIN quiz_category_join ON category.id=quiz_category_join.categoryId WHERE " +
            "quiz_category_join.quizId=:quizId")
    fun getCategoryForQuiz(quizId: Long): List<CategoryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQuizCategory(quizCategoryEntity: QuizCategoryEntity)



}