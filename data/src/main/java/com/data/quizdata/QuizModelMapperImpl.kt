package com.data.quizdata

import com.data.categoriesdata.CategoriesEntity
import com.data.categorydata.CategoryEntity
import com.data.tag.TagEntity
import domain.categories.Categories
import domain.category.Category
import domain.quiz.Quiz
import domain.tag.Tag

class QuizModelMapperImpl : QuizModelMapper {
    override fun toEntity(quiz: Quiz): QuizEntity = quiz.run {
        QuizEntity(id!!, buttonStart, shareTitle, questions, createdAt, sponsored, title, type, content)
    }


    override fun fromEntity(quizEntity: QuizEntity): Quiz = quizEntity.run {
        Quiz(buttonStart, shareTitle, questions, createdAt, sponsored, categories?.map { fromEntity(it) }, id, title, type, content)
    }

    override fun toEntity(categories: Categories): CategoriesEntity = categories.run {
        CategoriesEntity(uid, secondaryCid, name, type)
    }

    override fun fromEntity(categoriesEntity: CategoriesEntity): Categories = categoriesEntity.run {
        Categories(uid, secondaryCid, name, type)
    }

    override fun toEntity(tag: Tag): TagEntity = tag.run {
        TagEntity(uid, name, type)
    }

    override fun fromEntity(tagEntity: TagEntity): Tag = tagEntity.run {
        Tag(uid, name, type)
    }

    override fun toEntity(category: Category): CategoryEntity = category.run {
        CategoryEntity(id, name)
    }

    override fun fromEntity(categoryEntity: CategoryEntity): Category = categoryEntity.run {
        Category(id, name)
    }



}