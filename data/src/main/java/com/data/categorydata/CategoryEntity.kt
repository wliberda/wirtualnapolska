package com.data.categorydata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "category")
data class CategoryEntity(@PrimaryKey
                          var id: Long? = null,
                          var name: String? = null)