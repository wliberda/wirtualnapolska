package com.data.categorydata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import com.data.quizdata.QuizEntity


@Entity(tableName = "quiz_category_join",
        primaryKeys = ["quizId", "categoryId"],
        foreignKeys = [
            (ForeignKey(entity = QuizEntity::class,
                    parentColumns = ["id"],
                    childColumns = ["quizId"])),
            (ForeignKey(entity = CategoryEntity::class,
                    parentColumns = ["id"],
                    childColumns = ["categoryId"]))
        ]
)
data class QuizCategoryEntity(val quizId: Long, val categoryId: Long)