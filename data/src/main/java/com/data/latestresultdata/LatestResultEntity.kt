package com.data.latestresultdata

import android.arch.persistence.room.Entity

@Entity(tableName = "latest_result")
data class LatestResultEntity(var city: Int? = null,
                              var endDate: String? = null,
                              var result: Double? = null,
                              var resolveTime: Int? = null) {
}