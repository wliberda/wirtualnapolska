package com.data.imagedata

import android.arch.persistence.room.Entity

@Entity(tableName = "image")
data class ImageEntity(var author: String? = null,
                       var width: Int? = null,
                       var height: Int? = null,
                       var mediaId: String? = null,
                       var source: String? = null,
                       var url: String? = null) {
}