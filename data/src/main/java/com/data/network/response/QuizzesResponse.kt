package com.data.network.response

import com.data.quizdata.QuizEntity

data class QuizzesResponse(val count: Int,
                           val items: List<QuizEntity>) {
}