package com.data.network

import com.data.network.response.QuizzesResponse
import com.data.quizdata.QuizEntity
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface QuizAPI {

    @GET("api/v1/quizzes/0/100")
    fun getAllQuizzes(): Observable<Response<QuizzesResponse>>

    @GET("api/v1/quiz/{id}/0")
    fun getQuiz(@Path("id") id: Int): Observable<Response<QuizEntity>>
}