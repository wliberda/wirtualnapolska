package com.data.sponsoredresultdata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.data.imagedata.ImageEntity

@Entity(tableName = "sponsored_result")
data class SponsoredResultEntity(
//        @Ignore
//                                 var imageAuthor: ImageEntity? = null,
                                 var imageHeight: Int? = null,
                                 var imageUrl: String? = null,
                                 var imageWidth: Int? = null,
                                 var textColor: String? = null,
                                 var content: String? = null,
                                 var mainColor: String? = null,
                                 var imageSource: String? = null) {
}