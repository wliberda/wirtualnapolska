package com.data.celebritydata

import android.arch.persistence.room.Entity

@Entity(tableName = "celebrity")
data class CelebrityEntity(var result: String? = null,
                           var imageAuthor: String? = null,
                           var imageHeight: Int? = null,
                           var imageUrl: String? = null,
                           var show: Int? = null,
                           var name: String? = null,
                           var imageTitle: String? = null,
                           var imageWidth: Int? = null,
                           var content: String? = null,
                           var imageSource: String? = null) {
}