package com.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.data.categoriesdata.CategoriesEntity
import com.data.categoriesdata.QuizCategoriesEntity
import com.data.categorydata.CategoryEntity
import com.data.categorydata.QuizCategoryEntity
import com.data.quizdata.QuizDAO
import com.data.quizdata.QuizEntity
import com.data.tag.QuizTagEntity
import com.data.tag.TagEntity

@Database(entities = [
    (QuizEntity::class),
    (CategoriesEntity::class),
    (QuizCategoriesEntity::class),
    (TagEntity::class),
    (QuizTagEntity::class),
    (CategoryEntity::class),
    (QuizCategoryEntity::class)],
        version = 1,
        exportSchema = false
)
abstract class WpDb : RoomDatabase(){
    abstract fun quizDAO(): QuizDAO

    companion object {
        var INSTANCE: WpDb? = null
        fun getInstance(context: Context): WpDb? {
            if(INSTANCE == null) {
                synchronized(WpDb::class) {
                    INSTANCE = Room.databaseBuilder(context, WpDb::class.java, "wpddb").build()
                }
            }
            return INSTANCE
        }
    }
}