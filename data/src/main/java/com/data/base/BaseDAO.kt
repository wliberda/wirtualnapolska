package com.data.base

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy

interface BaseDAO<in T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(obj: T)

    @Delete
    fun delete(obj: T)
}