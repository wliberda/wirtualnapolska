package com.data.answerdata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.data.imagedata.ImageEntity
import domain.image.Image

@Entity(tableName = "answer")
data class AnswerEntity(
//        @Ignore
//                        var image: ImageEntity? = null,
                        var order: Int? = null,
                        @PrimaryKey
                        var text: String? = null,
                        var isCorrect: Int? = null) {
}