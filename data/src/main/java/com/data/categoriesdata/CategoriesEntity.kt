package com.data.categoriesdata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "categories")
class CategoriesEntity(@PrimaryKey
                       var uid: Long? = null,
                       var secondaryCid: String? = null,
                       var name: String? = null,
                       var type: String? = null) {
}