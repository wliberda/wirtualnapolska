package com.data.categoriesdata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import com.data.quizdata.QuizEntity


@Entity(tableName = "quiz_categories_join",
        primaryKeys = ["quizId", "categoriesId"],
        foreignKeys = [
            (ForeignKey(entity = QuizEntity::class,
                    parentColumns = ["id"],
                    childColumns = ["quizId"])),
            (ForeignKey(entity = CategoriesEntity::class,
                    parentColumns = ["uid"],
                    childColumns = ["categoriesId"]))]
)
data class QuizCategoriesEntity(val quizId: Long, val categoriesId: Long)