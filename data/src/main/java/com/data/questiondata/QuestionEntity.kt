package com.data.questiondata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import com.data.answerdata.AnswerEntity
import com.data.imagedata.ImageEntity
import domain.answer.Answer
import domain.image.Image

@Entity(tableName = "question")
data class QuestionEntity(var text: String? = null,
                          var answer: String? = null,
                          var type: String? = null,
                          var order: Int? = null) {
//                          @Ignore
//                          var image: ImageEntity? = null,
//                          @Ignore
//                          var answers: List<AnswerEntity>? = null ) {
}