package com.data.tag

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "tag")
data class TagEntity(
        @PrimaryKey
        var uid: Long? = null,
        var name: String? = null,
        var type: String? = null) {
}