package com.data.tag

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import com.data.quizdata.QuizEntity


@Entity(tableName = "quiz_tag_join",
        primaryKeys = ["quizId", "tagId"],
        foreignKeys = [
            (ForeignKey(entity = QuizEntity::class,
                    parentColumns = ["id"],
                    childColumns = ["quizId"])),
            (ForeignKey(entity = TagEntity::class,
                    parentColumns = ["uid"],
                    childColumns = ["tagId"]))]
)
data class QuizTagEntity(val quizId: Long, val tagId: Long)