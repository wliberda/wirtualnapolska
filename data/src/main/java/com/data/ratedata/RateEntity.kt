package com.data.ratedata

import android.arch.persistence.room.Entity

@Entity(tableName = "rate")
data class RateEntity(var from: Int? = null,
                      var to: Int? = null,
                      var content: String? = null)