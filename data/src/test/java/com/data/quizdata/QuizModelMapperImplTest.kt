package com.data.quizdata

import com.data.factory.QuizFactory.Factory.makeCategories
import com.data.factory.QuizFactory.Factory.makeCategoriesEntity
import com.data.factory.QuizFactory.Factory.makeCategory
import com.data.factory.QuizFactory.Factory.makeCategoryEntity
import com.data.factory.QuizFactory.Factory.makeTag
import com.data.factory.QuizFactory.Factory.makeTagEntity
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test


class QuizModelMapperImplTest {

    lateinit var testSubject: QuizModelMapperImpl


    @Before
    fun setUp() {
         testSubject = QuizModelMapperImpl()
    }

    @Test
    fun `subject cannot be null`() {
        assertNotNull(testSubject)
    }


    @Test
    fun `categories model should have the same values as categories entity`() {
        val categories = makeCategories()
        val categoriesEntity = testSubject.toEntity(categories)
        assertTrue(categories.name == categoriesEntity.name)
        assertTrue(categories.secondaryCid == categoriesEntity.secondaryCid)
        assertTrue(categories.type == categoriesEntity.type)
        assertTrue(categories.uid == categoriesEntity.uid)
    }


    @Test
    fun `categories entity should have the same values as model`() {
        val categoriesEntity = makeCategoriesEntity()
        val categories = testSubject.fromEntity(categoriesEntity)
        assertTrue(categories.name == categoriesEntity.name)
        assertTrue(categories.secondaryCid == categoriesEntity.secondaryCid)
        assertTrue(categories.type == categoriesEntity.type)
        assertTrue(categories.uid == categoriesEntity.uid)
    }


    @Test
    fun `tags entity should have the same values as tag model`() {
        val tag = makeTag()
        val tagEntity = testSubject.toEntity(tag)
        assertTrue(tag.name == tagEntity.name)
        assertTrue(tag.uid == tagEntity.uid)
        assertTrue(tag.type == tagEntity.type)
    }

    @Test
    fun `tags model should have the same values as tag entity`() {
        val tagEntity = makeTagEntity()
        val tag = testSubject.fromEntity(tagEntity)
        assertTrue(tag.name == tagEntity.name)
        assertTrue(tag.uid == tagEntity.uid)
        assertTrue(tag.type == tagEntity.type)
    }


    @Test
    fun `category model should have same values as category entity`() {
        val categoryEntity = makeCategoryEntity()
        val category = testSubject.fromEntity(categoryEntity)
        assertTrue(category.id == categoryEntity.id)
        assertTrue(category.name == categoryEntity.name)
    }


    @Test
    fun `category entity should have same values as category model`() {
        val category = makeCategory()
        val categoryEntity = testSubject.fromEntity(category)
        assertTrue(category.id == categoryEntity.id)
        assertTrue(category.name == categoryEntity.name)
    }
}