package com.data.factory

import java.util.*

class DataFactory {
    companion object Factory {
        fun randomString() = UUID.randomUUID().toString()
        fun randomUUID() = UUID.randomUUID()
        fun randomInt() = Random().nextInt()
        fun randomDouble() = Random().nextDouble()
        fun randomBoolean() = Random().nextBoolean()
        fun randomLong() = Random().nextLong()
    }
}