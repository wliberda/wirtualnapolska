package com.data.factory

import com.data.categoriesdata.CategoriesEntity
import com.data.categorydata.CategoryEntity
import com.data.factory.DataFactory.Factory.randomBoolean
import com.data.factory.DataFactory.Factory.randomInt
import com.data.factory.DataFactory.Factory.randomLong
import com.data.factory.DataFactory.Factory.randomString
import com.data.tag.TagEntity
import domain.categories.Categories
import domain.category.Category
import domain.image.Image
import domain.quiz.Quiz
import domain.tag.Tag

class QuizFactory {

    companion object Factory {
        fun makeQuizList(count: Int): List<Quiz> {
            val list = mutableListOf<Quiz>()
            repeat(count, {
                list.add(makeQuiz())
            })
            return list
        }

        fun makeQuiz() = Quiz(
                randomString(),
                randomString(),
                randomInt(),
                randomString(),
                randomBoolean(),
                makeCategoriesList(3),
                randomLong(),
                randomString(),
                randomString(),
                randomString(),
                Image(),
                mutableListOf(),
                Category()
        )

        fun makeCategoriesList(count: Int): List<Categories> {
            val list = mutableListOf<Categories>()
            repeat(count, {
                list.add(makeCategories())
            })
            return list
        }

        fun makeCategories() = Categories(
                randomLong(),
                randomString(),
                randomString(),
                randomString()
        )


        fun makeCategoriesEntityListy(count: Int): List<CategoriesEntity> {
            val list = mutableListOf<CategoriesEntity>()
            repeat(count, {
                list.add(makeCategoriesEntity())
            })
            return list
        }

        fun makeCategoriesEntity() = CategoriesEntity(randomLong(),
                randomString(),
                randomString(),
                randomString())

        fun makeTagList(count: Int): List<Tag> {
            val list = mutableListOf<Tag>()
            repeat(count, {
                list.add(makeTag())
            })
            return list
        }


        fun makeTag() = Tag(randomLong(), randomString(), randomString())

        fun makeTagEntityList(count: Int): List<TagEntity> {
            val list = mutableListOf<TagEntity>()
            repeat(count, {
                list.add(makeTagEntity())
            })
            return list
        }

         fun makeTagEntity(): TagEntity = TagEntity(randomLong(), randomString(), randomString())


        fun makeCategoryEntity() = CategoryEntity(randomLong(), randomString())
        fun makeCategory() = Category(randomLong(), randomString())

    }


}