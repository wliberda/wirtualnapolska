package domain.quiz.factory

import domain.quiz.Quiz
import domain.quiz.factory.DataFactory.Factory.randomBoolean
import domain.quiz.factory.DataFactory.Factory.randomDouble
import domain.quiz.factory.DataFactory.Factory.randomInt
import domain.quiz.factory.DataFactory.Factory.randomString

class QuizFactory {

    companion object Factory {


        fun makeQuizList(count: Int): List<Quiz> {
            val list = mutableListOf<Quiz>()
            repeat(count, {
                list.add(makeQuiz())
            })
            return list
        }

        fun makeQuiz() = Quiz(
                randomString(),
                randomBoolean(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomInt(),
                randomString(),
                randomInt(),
                randomDouble(),
                randomInt(),
                randomBoolean()
        )
    }
}