package domain.quiz.usecase

import com.nhaarman.mockito_kotlin.*
import domain.quiz.Quiz
import domain.quiz.QuizDetail
import domain.quiz.QuizRepository
import io.reactivex.Maybe
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test


class GetQuizDetailUseCaseTest {

    val repository: QuizRepository = mock()
    lateinit var testSubject: GetQuizDetailUseCase

    @Before
    fun setUp() {
        testSubject = GetQuizDetailUseCase(repository)
    }

    @Test
    fun `subject cannot be null`() {
        assertNotNull(testSubject)
    }


    @Test
    fun `should return QuizDetail when valid param`() {
        val quizDetail = QuizDetail()
        whenever(repository.getQuiz(any())).thenReturn(Maybe.just(quizDetail))
        val testObserver = testSubject.buildMaybeUseCase(4L).test()
        testObserver.assertValue(quizDetail)
        verify(repository).getQuiz(any())
        assertTrue(testObserver.hasSubscription())
        testObserver.assertComplete()

    }

    @Test
    fun `should return IllegalArgumentException when notValid param`() {
        val quizDetail = QuizDetail()
        whenever(repository.getQuiz(any())).thenReturn(Maybe.just(quizDetail))
        val testObserver = testSubject.buildMaybeUseCase(null).test()
        testObserver.assertError(IllegalArgumentException::class.java)
        verifyZeroInteractions(repository)
        assertTrue(testObserver.hasSubscription())

    }
}