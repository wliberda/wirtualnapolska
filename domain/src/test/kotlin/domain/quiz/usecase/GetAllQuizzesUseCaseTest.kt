package domain.quiz.usecase

import com.nhaarman.mockito_kotlin.*
import com.data.factory.QuizFactory.Factory.makeQuizList
import domain.quiz.QuizRepository
import io.reactivex.Single
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Test


class GetAllQuizzesUseCaseTest {
    private val repository: QuizRepository = mock()
    private lateinit var testSubject: GetAllQuizzesUseCase

    @Before
    fun setUp() {
        testSubject = GetAllQuizzesUseCase(repository)
    }

    @Test
    fun `subject shouldnt be null`() {
        assertNotNull(testSubject)
    }


    @Test
    fun `should return List`() {
        val list = makeQuizList(5)
        val single = Single.just(list)
        whenever(repository.getAllQuiz()).thenReturn(single)
        val testObserver = testSubject.buildSingeUseCase(null).test()
        testObserver.assertValue(list)
        verify(repository, times(1)).getAllQuiz()
        verifyNoMoreInteractions(repository)
        assertTrue(testObserver.hasSubscription())
        testObserver.assertComplete()
    }


}