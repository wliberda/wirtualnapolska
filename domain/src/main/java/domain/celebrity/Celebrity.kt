package domain.celebrity

data class Celebrity(val result: String? = null,
                     val imageAuthor: String? = null,
                     val imageHeight: Int? = null,
                     val imageUrl: String? = null,
                     val show: Int? = null,
                     val name: String? = null,
                     val imageTitle: String? = null,
                     val imageWidth: Int? = null,
                     val content: String? = null,
                     val imageSource: String? = null) {
}