package domain.base

import io.reactivex.Maybe
import io.reactivex.disposables.Disposables
import io.reactivex.observers.DisposableMaybeObserver

abstract class MaybeUseCase<T, in Params> {

    private var disposable = Disposables.empty()

    abstract fun buildMaybeUseCase(param: Params?) : Maybe<T>

    open fun execute(observer: DisposableMaybeObserver<T>, params: Params?) {
        disposable = buildMaybeUseCase(params).subscribeWith(observer)
    }

    fun dispose() {
        if(!disposable.isDisposed)
            disposable.dispose()
    }
}