package domain.base

import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.observers.DisposableCompletableObserver

abstract class CompletableUseCase<in Params> {
    private var disposable = Disposables.empty()

    abstract fun buildUseCaseComplete(param: Params?): Completable

    open fun execute(observer: DisposableCompletableObserver, params: Params? = null) {
        disposable = buildUseCaseComplete(params).subscribeWith(observer)
    }

    fun dispose() {
        if (!disposable.isDisposed) disposable.dispose()
    }}