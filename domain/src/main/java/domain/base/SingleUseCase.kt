package domain.base

import io.reactivex.Single
import io.reactivex.disposables.Disposables
import io.reactivex.observers.DisposableSingleObserver

abstract class SingleUseCase<T, in Params> {
    private var disposable = Disposables.empty()

    abstract fun buildSingeUseCase(params: Params?): Single<T>

    open fun execute(observer: DisposableSingleObserver<T>, params: Params?) {
        disposable = buildSingeUseCase(params).subscribeWith(observer)
    }

    fun dispose() {
        if(!disposable.isDisposed)
            disposable.dispose()
    }

}