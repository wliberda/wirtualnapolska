package domain.latestresult

data class LatestResult(val city: Int? = null,
                        val endDate: String? = null,
                        val result: Double? = null,
                        val resolveTime: Int? = null) {
}