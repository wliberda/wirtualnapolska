package domain.quiz

import domain.categories.Categories
import domain.category.Category
import domain.celebrity.Celebrity
import domain.image.Image
import domain.latestresult.LatestResult
import domain.question.Question
import domain.rate.Rates
import domain.sponsoredresult.SponsoredResult
import domain.tag.Tag

data class Quiz(var buttonStart: String? = null,
                var shareTitle: String? = null,
                var questions: Int? = null,
                var createdAt: String? = null,
                var sponsored: Boolean? = null,
                var categories: List<Categories>? = null,
                var id: Long? = null,
                var title: String? = null,
                var type: String? = null,
                var content: String? = null,
                var mainPhoto: Image? = null,
                var tags: List<Tag>? = null,
                var category: Category? = null)