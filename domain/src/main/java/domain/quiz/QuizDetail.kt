package domain.quiz

import com.sun.org.apache.xpath.internal.operations.Bool
import domain.categories.Categories
import domain.category.Category
import domain.celebrity.Celebrity
import domain.image.Image
import domain.latestresult.LatestResult
import domain.question.Question
import domain.rate.Rates
import domain.sponsoredresult.SponsoredResult
import domain.tag.Tag

data class QuizDetail(var createdAt: Long? = null,
                      var sponsored: Boolean? = null,
                      var title: String? = null,
                      var type: String? = null,
                      var content: String? = null,
                      var buttonStart: String? = null,
                      var shareTitle: String? = null,
                      var id: Long? = null,
                      var scripts: String? = null,
                      var created: Int? = null,
                      var avgResult: Double? = null,
                      var resultCount: Int? = null,
                      var userBattleDone: Boolean? = null,
                      var isBattle: Boolean? = null,
                      var tags: List<Tag>? = null,
                      var questions: List<Question>? = null,
                      var rates: List<Rates>? = null,
                      var category: Category? = null,
                      var categories: List<Categories>? = null,
                      var mainPhoto: Image? = null,
                      var latestResult: List<LatestResult>? = null,
                      var sponsoredResults: SponsoredResult? = null,
                      var celebrity: Celebrity? = null) {
}