package domain.quiz

import domain.quiz.Quiz
import io.reactivex.Maybe
import io.reactivex.Single

interface QuizRepository {

    fun getAllQuiz(): Single<List<Quiz>>
    fun getQuiz(id: Long): Maybe<QuizDetail>
}