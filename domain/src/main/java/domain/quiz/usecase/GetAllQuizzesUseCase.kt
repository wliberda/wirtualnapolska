package domain.quiz.usecase

import domain.base.SingleUseCase
import domain.quiz.Quiz
import domain.quiz.QuizRepository
import io.reactivex.Single

open class GetAllQuizzesUseCase(private val repository: QuizRepository): SingleUseCase<List<Quiz>, Void>() {

    override fun buildSingeUseCase(params: Void?): Single<List<Quiz>> = repository.getAllQuiz()


}