package domain.quiz.usecase

import domain.base.MaybeUseCase
import domain.quiz.QuizDetail
import domain.quiz.QuizRepository
import io.reactivex.Completable
import io.reactivex.Maybe

open class GetQuizDetailUseCase(private val quizRepository: QuizRepository) : MaybeUseCase<QuizDetail, Long>() {
    override fun buildMaybeUseCase(param: Long?): Maybe<QuizDetail> =
            if (param == null) Maybe.error(IllegalArgumentException())
            else quizRepository.getQuiz(param)
//        valid(param).andThen(quizRepository.getQuiz(param!!))


}