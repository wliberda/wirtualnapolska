package domain.categories

data class Categories(val uid: Long? = null,
                      val secondaryCid: String? = null,
                      val name: String? = null,
                      val type: String? = null) {
}