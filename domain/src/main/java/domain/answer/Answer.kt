package domain.answer

import domain.image.Image

data class Answer(val image: Image? = null,
                  val order: Int? = null,
                  val text: String? = null,
                  val isCorrect: Int? = null) {
}