package domain.image

data class Image(val author: String? = null,
                 val width: Int? = null,
                 val height: Int? = null,
                 val mediaId: String? = null,
                 val source: String? = null,
                 val url: String? = null) {
}