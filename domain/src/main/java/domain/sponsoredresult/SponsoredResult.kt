package domain.sponsoredresult

import domain.image.Image

data class SponsoredResult(val imageAuthor: Image? = null,
                           val imageHeight: Int? = null,
                           val imageUrl: String? = null,
                           val imageWidth: Int? = null,
                           val textColor: String? = null,
                           val content: String? = null,
                           val mainColor: String? = null,
                           val imageSource: String? = null) {
}