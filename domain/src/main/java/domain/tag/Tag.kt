package domain.tag

data class Tag(var uid: Long? = null,
               var name: String? = null,
               var type: String? = null) {
}