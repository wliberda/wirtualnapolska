package domain.rate

data class Rates(val from: Int? = null,
                 val to: Int? = null,
                 val content: String? = null) {
}