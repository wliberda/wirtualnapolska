package domain.category

data class Category(val id: Long? = null,
                    val name: String? = null) {
}