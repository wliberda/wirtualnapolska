package domain.question

import domain.answer.Answer
import domain.image.Image

data class Question(val image: Image? = null,
                    val answers: List<Answer>? = null,
                    val text: String? = null,
                    val answer: String? = null,
                    val type: String? = null,
                    val order: Int? = null
)
